import sys
import math
import random

# Bring data on patient samples from the diagnosis machine to the laboratory with enough molecules to produce medicine!

# ACTIONS
COLLECT = 'collect'
ANALYZE = 'analyze'
UPLOAD = 'upload'
DOWNLOAD = 'download'
GATHER = 'gather'
PRODUCE = 'produce'

# PRIORITIES
COLLECT_PRIORITY = 2
COLLECT_FAR_PRIORITY = 1
ANALYZE_PRIORITY = 9
ANALYZE_FAR_PRIORITY = 1
DOWNLOAD_PRIORITY = 3
UPLOAD_PRIORITY = 4
GATHER_PRIORITY = 8
PRODUCE_PRIORITY = 6

# Lab modules
SAMPLES = 'SAMPLES'
DIAGNOSIS = 'DIAGNOSIS'
MOLECULES = 'MOLECULES'
LABORATORY = 'LABORATORY'

# COMMANDS
GOTO = 'GOTO {0}'
CONNECT = 'CONNECT {0}'
WAIT = 'WAIT'

# GLOBAL VARIABLES
my_robot = {}
phase_index = 0
my_robot_fails = 0


class SampleData:
    def __init__(self, sample_id, carried_by, rank, expertise_gain, health, cost_a, cost_b, cost_c, cost_d, cost_e):
        self.id = sample_id
        self.carried_by = carried_by
        self.rank = rank
        self.expertise_gain = expertise_gain
        self.health = health
        self.costs = {
            'A': cost_a,
            'B': cost_b,
            'C': cost_c,
            'D': cost_d,
            'E': cost_e
        }

    def get_molecules_quantity(self):
        return sum(cost for cost in self.costs.values())

    def is_undiagnosed(self):
        return self.health == -1


class DiagnosisModule:
    samples = []

    def add_sample(self, sample):
        self.samples = self.samples + [sample]


class MoleculesModule:
    def __init__(self, available_a, available_b, available_c, available_d, available_e):
        self.availables = {
            'A': available_a,
            'B': available_b,
            'C': available_c,
            'D': available_d,
            'E': available_e
        }


class Robot:
    max_molecule_storage = 10
    max_sample_storage = 3
    samples = []
    actions = []

    def __init__(self, target, storage_a, storage_b, storage_c, storage_d, storage_e,
                 expertise_a, expertise_b, expertise_c, expertise_d, expertise_e):
        self.target = target
        self.storages = {
            'A': storage_a,
            'B': storage_b,
            'C': storage_c,
            'D': storage_d,
            'E': storage_e
        }
        self.expertises = {
            'A': expertise_a,
            'B': expertise_b,
            'C': expertise_c,
            'D': expertise_d,
            'E': expertise_e
        }

    @staticmethod
    def fail():
        global my_robot_fails
        my_robot_fails += 1

    @staticmethod
    def success():
        global my_robot_fails
        if my_robot_fails > 0:
            my_robot_fails -= 1

    @staticmethod
    def get_rank_correction():
        global my_robot_fails
        return - (my_robot_fails // 3)

    def add_sample(self, sample):
        self.samples = self.samples + [sample]

    def get_molecule_available_storage(self):
        return self.max_molecule_storage - sum(storage for storage in self.storages.values())

    def get_sample_available_storage(self):
        return self.max_sample_storage - len(self.samples)

    def get_molecule_expected_available_storage(self):
        return self.max_molecule_storage - sum(s.get_molecules_quantity() for s in self.samples)

    def get_next_needed_available_molecule(self, availables):
        all_availables = dict([(mol_type, available + self.storages[mol_type] + self.expertises[mol_type])
                               for (mol_type, available) in availables.items()])
        selected_sample = None
        for s in self.samples:
            if not s.is_undiagnosed() and self.enough_molecules_to_produce_sample(s, all_availables):
                selected_sample = s
                break

        if selected_sample is None:
            return None
        else:
            needed_available_molecule = None
            for (mol_type, cost) in selected_sample.costs.items():
                if cost - self.storages[mol_type] > 0 and availables[mol_type] > 0:
                    needed_available_molecule = mol_type

            return needed_available_molecule

    def get_first_sample(self):
        if len(self.samples) > 0:
            return self.samples[0]
        else:
            return None

    @staticmethod
    def enough_molecules_to_produce_sample(sample, molecules):
        return not sample.is_undiagnosed() \
               and all([molecules[mol_type] - quantity >= 0 for (mol_type, quantity) in sample.costs.items()])

    def enough_molecules_to_produce_my_sample(self, availables):
        all_availables = dict([(mol_type, num + self.storages[mol_type] + self.expertises[mol_type])
                               for (mol_type, num) in availables.items()])
        is_enough = False
        for s in self.samples:
            if not s.is_undiagnosed() and self.enough_molecules_to_produce_sample(s, all_availables):
                is_enough = True
                break
        return is_enough

    def is_sample_ready_to_produce(self, sample):
        all_molecules = dict([(mol_type, num + self.expertises[mol_type]) for (mol_type, num) in self.storages.items()])
        return self.enough_molecules_to_produce_sample(sample, all_molecules)

    def get_my_sample_ready_to_produce(self):
        ready_sample = None
        for s in self.samples:
            if self.is_sample_ready_to_produce(s):
                ready_sample = s
                break
        return ready_sample

    def get_undiagnosed_sample(self):
        undiagnosed_sample = None
        for s in self.samples:
            if s.is_undiagnosed():
                undiagnosed_sample = s
                break

        return undiagnosed_sample

    def calculate_remaining_molecules(self, molecules):
        molecules_copy = dict([(mol_type, num) for (mol_type, num) in molecules.items()])
        for sample in self.samples:
            for (mol_type, num) in sample.costs.items():
                molecules_copy[mol_type] -= num
        return molecules_copy

    def get_best_sample(self, samples, availables):
        best_sample = None
        all_availables = dict([(mol_type, num + self.storages[mol_type] + self.expertises[mol_type])
                               for (mol_type, num) in availables.items()])
        for s in samples:
            if self.enough_molecules_to_produce_sample(s, all_availables) \
                and s.get_molecules_quantity() < self.get_molecule_available_storage() \
                    and (best_sample is None or s.health > best_sample.health):
                best_sample = s

        return best_sample

    def add_action(self, name, diagnosis_mod, molecules_mod, constraint_func, priority_func, execute_func):
        self.actions = self.actions + [RobotAction(name, self, diagnosis_mod, molecules_mod, constraint_func, priority_func, execute_func)]

    def do_something(self):
        high_priority_action = None
        for action in self.actions:
            if action.get_constraint():
                if high_priority_action is None\
                        or action.get_priority() > high_priority_action.get_priority():
                    high_priority_action = action

        if high_priority_action is not None:
            return high_priority_action.execute()
        elif self.target != MOLECULES:
            return GOTO.format(MOLECULES)
        else:
            return WAIT


class RobotAction:
    def __init__(self, name, robot, diagnosis_mod, molecules_mod, constraint_func, priority_func, execute_func):
        self.name = name
        self.robot = robot
        self.diagnosis_mod = diagnosis_mod
        self.molecules_mod = molecules_mod
        self.constraint_func = constraint_func
        self.priority_func = priority_func
        self.execute_func = execute_func

    def get_priority(self):
        return self.priority_func(self)

    def get_constraint(self):
        return self.constraint_func(self)

    def execute(self):
        return self.execute_func(self)