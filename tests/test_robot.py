import unittest

from models import Robot
from models import SampleData
from models import DiagnosisModule


class TestRobotMethods(unittest.TestCase):
    availables = {
        'A': 6,
        'B': 6,
        'C': 6,
        'D': 6,
        'E': 6
    }

    def _collect_constraint(self, my_robot):
        return my_robot.get_sample_available_storage() > 0

    def _analyze_constraint(self, my_robot):
        return my_robot.get_undiagnosed_sample() is not None

    def _download_constraint(self, my_robot, diagnosis_samples, availables):
        return my_robot.get_sample_available_storage() > 0 \
               and my_robot.get_best_sample(diagnosis_samples, availables) is not None

    def _gather_constraint(self, my_robot, availables):
        return my_robot.get_molecule_available_storage() > 0 \
               and my_robot.enough_molecules_to_produce_my_sample(availables) is not None \
               and my_robot.get_next_needed_available_molecule(availables) is not None

    def _upload_constraint(self, my_robot, diagnosis_samples, availables):
        return any(not s.is_undiagnosed() for s in my_robot.samples) \
               and not any([self._collect_constraint(my_robot),
                            self._analyze_constraint(my_robot),
                            self._download_constraint(my_robot, diagnosis_samples, availables),
                            self._gather_constraint(my_robot, availables),
                            self._produce_constraint(my_robot)])

    def _produce_constraint(self, my_robot):
        return my_robot.get_my_sample_ready_to_produce() is not None

    def test_get_next_needed_available_molecule(self):
        availables = {'A': 6, 'B': 6, 'C': 6, 'D': 5, 'E': 6}
        my_robot = Robot('MOLECULES', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0)

        # no samples
        self.assertEqual(my_robot.get_next_needed_available_molecule(availables), None)

        # sample = (0,0,0,6,0), availables = (6,6,6,5,6), storages = (0,0,0,1,0)
        my_sample = SampleData(1, 1, 1, 1, 1, 0, 0, 0, 6, 0)
        my_robot.add_sample(my_sample)
        self.assertEqual(my_robot.get_next_needed_available_molecule(availables), 'D')

        my_robot = Robot('MOLECULES', 0, 0, 0, 0, 0, 2, 1, 1, 1, 1)
        my_sample = SampleData(1, 1, 1, 1, 1, 0, 7, 3, 0, 0)
        my_robot.add_sample(my_sample)
        availables = {'A': 6, 'D': 6, 'B': 6, 'E': 6, 'C': 6}
        self.assertNotEqual(my_robot.get_next_needed_available_molecule(availables), None)

    def test_upload_constraint(self):
        my_robot = Robot('MOLECULES', 0, 1, 3, 3, 3, 0, 0, 0, 0, 0)
        my_sample = SampleData(1, 1, 1, 1, 1, 0, 0, 3, 6, 3)
        my_robot.add_sample(my_sample)
        my_sample = SampleData(1, 1, 1, 1, 1, 1, 3, 1, 0, 0)
        my_robot.add_sample(my_sample)
        my_sample = SampleData(1, 1, 1, 1, 1, 2, 0, 0, 1, 4)
        my_robot.add_sample(my_sample)
        availables = {'A': 6, 'B': 5, 'C': 3, 'D': 3, 'E': 3}
        self.assertTrue(self._upload_constraint(my_robot, DiagnosisModule(), availables))

        my_robot = Robot('MOLECULES', 0, 0, 2, 0, 0, 0, 3, 3, 2, 0)
        my_sample = SampleData(1, 1, 1, 1, 1, 0, 0, 0, 0, 6)
        my_robot.add_sample(my_sample)
        my_sample = SampleData(1, 1, 1, 1, 1, 0, 3, 3, 5, 3)
        my_robot.add_sample(my_sample)
        my_sample = SampleData(1, 1, 1, 1, 1, 0, 0, 3, 6, 3)
        my_robot.add_sample(my_sample)
        availables = {'B': 5, 'D': 3, 'A': 2, 'C': 3, 'E': 0}
        # self.assertTrue(self._upload_constraint(my_robot, DiagnosisModule(), availables))

    def test_gather_constraint(self):
        my_robot = Robot('MOLECULES', 0, 1, 3, 3, 3, 0, 0, 0, 0, 0)
        my_sample = SampleData(1, 1, 1, 1, 1, 0, 0, 3, 6, 3)
        my_robot.add_sample(my_sample)
        my_sample = SampleData(1, 1, 1, 1, 1, 1, 3, 1, 0, 0)
        my_robot.add_sample(my_sample)
        my_sample = SampleData(1, 1, 1, 1, 1, 2, 0, 0, 1, 4)
        my_robot.add_sample(my_sample)
        availables = {'A': 6, 'B': 5, 'C': 3, 'D': 3, 'E': 3}
        self.assertFalse(self._gather_constraint(my_robot, availables))

        my_robot = Robot('MOLECULES', 0, 0, 0, 0, 0, 2, 1, 1, 1, 1)
        my_sample = SampleData(1, 1, 1, 1, 1, 0, 7, 3, 0, 0)
        my_robot.add_sample(my_sample)
        availables = {'A': 6, 'D': 6, 'B': 6, 'E': 6, 'C': 6}
        self.assertTrue(self._gather_constraint(my_robot, availables))

    def test_get_rank_correction(self):
        my_robot = Robot('MOLECULES', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
        self.assertEqual(my_robot.get_rank_correction(), 0)

        my_robot.fail()
        self.assertEqual(my_robot.get_rank_correction(), 0)

        my_robot.fail()
        my_robot.fail()
        self.assertEqual(my_robot.get_rank_correction(), -1)

        my_robot.fail()
        self.assertEqual(my_robot.get_rank_correction(), -1)

        my_robot.fail()
        my_robot.fail()
        self.assertEqual(my_robot.get_rank_correction(), -2)

        my_robot.success()
        self.assertEqual(my_robot.get_rank_correction(), -1)
