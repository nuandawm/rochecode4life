phases = ['collecting', 'goto_molecules', 'gathering', 'goto_laboratory', 'producing', 'goto_diagnosis']
phase_index = 0


def next_phase():
    global phase_index
    phase_index = (phase_index + 1) % len(phases)

for i in range(10):
    print "Phase", phases[phase_index]
    next_phase()
