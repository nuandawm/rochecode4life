def get_next_needed_molecule(self):
    # This is really bad...
    needed = {
        'A': sum(s.costs['A'] for s in self.samples),
        'B': sum(s.costs['B'] for s in self.samples),
        'C': sum(s.costs['C'] for s in self.samples),
        'D': sum(s.costs['D'] for s in self.samples),
        'E': sum(s.costs['E'] for s in self.samples)
    }
    for (mol_type, need) in [(mol_type, need - self.storages[mol_type]) for (mol_type, need) in needed.items()]:
        if need > 0:
            return mol_type
    return None


class SampleData:
    def __init__(self, cost_a, cost_b, cost_c, cost_d, cost_e):
        self.costs = {
            'A': cost_a,
            'B': cost_b,
            'C': cost_c,
            'D': cost_d,
            'E': cost_e
        }


class Robot:
    def __init__(self, samples):
        self.storages = {
            'A': 0,
            'B': 0,
            'C': 0,
            'D': 0,
            'E': 0
        }

        self.samples = samples

my_robot = Robot([SampleData(2, 1, 2, 1, 3), SampleData(0, 1, 0, 0, 2)])

count = 0
while get_next_needed_molecule(my_robot) is not None:
    next_molecule = get_next_needed_molecule(my_robot)
    print next_molecule
    my_robot.storages[next_molecule] += 1
    count += 1

print "Count", count
