import sys
import math
import random

# Bring data on patient samples from the diagnosis machine to the laboratory with enough molecules to produce medicine!

# ACTIONS
COLLECT = 'collect'
ANALYZE = 'analyze'
UPLOAD = 'upload'
DOWNLOAD = 'download'
GATHER = 'gather'
PRODUCE = 'produce'

# PRIORITIES
COLLECT_PRIORITY = 2
COLLECT_FAR_PRIORITY = 1
ANALYZE_PRIORITY = 9
ANALYZE_FAR_PRIORITY = 1
DOWNLOAD_PRIORITY = 3
UPLOAD_PRIORITY = 4
GATHER_PRIORITY = 8
PRODUCE_PRIORITY = 6

# Lab modules
SAMPLES = 'SAMPLES'
DIAGNOSIS = 'DIAGNOSIS'
MOLECULES = 'MOLECULES'
LABORATORY = 'LABORATORY'

# COMMANDS
GOTO = 'GOTO {0}'
CONNECT = 'CONNECT {0}'
WAIT = 'WAIT'

# GLOBAL VARIABLES
my_robot = {}
my_robot_fails = 0
EXPERTISE_TO_SWITCH_TO_3RD_RANK = 3

def log(message):
    print(message, file=sys.stderr)


class SampleData:
    def __init__(self, sample_id, carried_by, rank, expertise_gain, health, cost_a, cost_b, cost_c, cost_d, cost_e):
        self.id = sample_id
        self.carried_by = carried_by
        self.rank = rank
        self.expertise_gain = expertise_gain
        self.health = health
        self.costs = {
            'A': cost_a,
            'B': cost_b,
            'C': cost_c,
            'D': cost_d,
            'E': cost_e
        }

    def get_molecules_quantity(self):
        return sum(cost for cost in self.costs.values())

    def is_undiagnosed(self):
        return self.health == -1


class DiagnosisModule:
    samples = []

    def add_sample(self, sample):
        self.samples = self.samples + [sample]


class MoleculesModule:
    def __init__(self, available_a, available_b, available_c, available_d, available_e):
        self.availables = {
            'A': available_a,
            'B': available_b,
            'C': available_c,
            'D': available_d,
            'E': available_e
        }


class Robot:
    max_molecule_storage = 10
    max_sample_storage = 3
    samples = []
    actions = []

    def __init__(self, target, storage_a, storage_b, storage_c, storage_d, storage_e,
                 expertise_a, expertise_b, expertise_c, expertise_d, expertise_e):
        self.target = target
        self.storages = {
            'A': storage_a,
            'B': storage_b,
            'C': storage_c,
            'D': storage_d,
            'E': storage_e
        }
        self.expertises = {
            'A': expertise_a,
            'B': expertise_b,
            'C': expertise_c,
            'D': expertise_d,
            'E': expertise_e
        }

    @staticmethod
    def fail():
        global my_robot_fails
        my_robot_fails += 1

    @staticmethod
    def success():
        global my_robot_fails
        if my_robot_fails > 0:
            my_robot_fails -= 1

    @staticmethod
    def get_rank_correction():
        global my_robot_fails
        return - (my_robot_fails // 3)

    def add_sample(self, sample):
        self.samples = self.samples + [sample]

    def get_molecule_available_storage(self):
        return self.max_molecule_storage - sum(storage for storage in self.storages.values())

    def get_sample_available_storage(self):
        return self.max_sample_storage - len(self.samples)

    def get_molecule_expected_available_storage(self):
        return self.max_molecule_storage - sum(s.get_molecules_quantity() for s in self.samples)

    def get_next_needed_available_molecule(self, availables):
        all_availables = dict([(mol_type, available + self.storages[mol_type] + self.expertises[mol_type])
                               for (mol_type, available) in availables.items()])
        selected_sample = None
        for s in self.samples:
            if not s.is_undiagnosed() and self.enough_molecules_to_produce_sample(s, all_availables):
                selected_sample = s
                break

        if selected_sample is None:
            return None
        else:
            needed_available_molecule = None
            for (mol_type, cost) in selected_sample.costs.items():
                if cost - self.storages[mol_type] - self.expertises[mol_type] > 0 and availables[mol_type] > 0:
                    needed_available_molecule = mol_type

            return needed_available_molecule

    def get_first_sample(self):
        if len(self.samples) > 0:
            return self.samples[0]
        else:
            return None

    @staticmethod
    def enough_molecules_to_produce_sample(sample, molecules):
        return not sample.is_undiagnosed() \
               and all([molecules[mol_type] - quantity >= 0 for (mol_type, quantity) in sample.costs.items()])

    def enough_molecules_to_produce_my_sample(self, availables):
        return self.get_next_needed_available_molecule(availables) is not None

    def is_sample_ready_to_produce(self, sample):
        all_molecules = dict([(mol_type, num + self.expertises[mol_type]) for (mol_type, num) in self.storages.items()])
        return self.enough_molecules_to_produce_sample(sample, all_molecules)

    def get_my_sample_ready_to_produce(self):
        ready_sample = None
        for s in self.samples:
            if self.is_sample_ready_to_produce(s):
                ready_sample = s
                break
        return ready_sample

    def get_undiagnosed_sample(self):
        undiagnosed_sample = None
        for s in self.samples:
            if s.is_undiagnosed():
                undiagnosed_sample = s
                break

        return undiagnosed_sample

    def calculate_remaining_molecules(self, molecules):
        molecules_copy = dict([(mol_type, num) for (mol_type, num) in molecules.items()])
        for sample in self.samples:
            for (mol_type, num) in sample.costs.items():
                molecules_copy[mol_type] -= num
        return molecules_copy

    def get_best_sample(self, samples, availables):
        best_sample = None
        all_availables = dict([(mol_type, num + self.storages[mol_type] + self.expertises[mol_type])
                               for (mol_type, num) in availables.items()])
        for s in samples:
            if self.enough_molecules_to_produce_sample(s, all_availables) \
                and s.get_molecules_quantity() < self.get_molecule_available_storage() \
                    and (best_sample is None or s.health > best_sample.health):
                best_sample = s

        return best_sample

    def add_action(self, name, diagnosis_mod, molecules_mod, constraint_func, priority_func, execute_func):
        self.actions = self.actions + [RobotAction(name, self, diagnosis_mod, molecules_mod, constraint_func, priority_func, execute_func)]

    def do_something(self):
        high_priority_action = None
        for action in self.actions:
            log(action.name + ' ' + str(action.get_constraint()) + ' ' + str(action.get_priority()))
            if action.get_constraint():
                if high_priority_action is None\
                        or action.get_priority() > high_priority_action.get_priority():
                    high_priority_action = action

        if high_priority_action is not None:
            return high_priority_action.execute()
        elif self.target != MOLECULES:
            return GOTO.format(MOLECULES)
        else:
            return WAIT


class RobotAction:
    def __init__(self, name, robot, diagnosis_mod, molecules_mod, constraint_func, priority_func, execute_func):
        self.name = name
        self.robot = robot
        self.diagnosis_mod = diagnosis_mod
        self.molecules_mod = molecules_mod
        self.constraint_func = constraint_func
        self.priority_func = priority_func
        self.execute_func = execute_func

    def get_priority(self):
        return self.priority_func(self)

    def get_constraint(self):
        return self.constraint_func(self)

    def execute(self):
        return self.execute_func(self)


# initial inputs
project_count = int(input())
for i in range(project_count):
    a, b, c, d, e = [int(j) for j in input().split()]

# game loop
while True:
    for i in range(2):
        target, eta, score, storage_a, storage_b, storage_c, storage_d, storage_e, expertise_a, expertise_b, expertise_c, expertise_d, expertise_e = input().split()
        eta = int(eta)
        score = int(score)
        storage_a = int(storage_a)
        storage_b = int(storage_b)
        storage_c = int(storage_c)
        storage_d = int(storage_d)
        storage_e = int(storage_e)
        expertise_a = int(expertise_a)
        expertise_b = int(expertise_b)
        expertise_c = int(expertise_c)
        expertise_d = int(expertise_d)
        expertise_e = int(expertise_e)

        if i == 0:
            my_robot = Robot(target, storage_a, storage_b, storage_c, storage_d, storage_e,
                             expertise_a, expertise_b, expertise_c, expertise_d, expertise_e)
        else:
            opponent_robot = Robot(target, storage_a, storage_b, storage_c, storage_d, storage_e,
                             expertise_a, expertise_b, expertise_c, expertise_d, expertise_e)

    available_a, available_b, available_c, available_d, available_e = [int(i) for i in input().split()]
    molecules = MoleculesModule(available_a, available_b, available_c, available_d, available_e)

    diagnosis = DiagnosisModule()
    sample_count = int(input())
    for i in range(sample_count):
        sample_id, carried_by, rank, expertise_gain, health, cost_a, cost_b, cost_c, cost_d, cost_e = input().split()
        sample_id = int(sample_id)
        carried_by = int(carried_by)
        rank = int(rank)
        health = int(health)
        cost_a = int(cost_a)
        cost_b = int(cost_b)
        cost_c = int(cost_c)
        cost_d = int(cost_d)
        cost_e = int(cost_e)

        if carried_by == -1:
            diagnosis.add_sample(SampleData(sample_id, carried_by, rank, expertise_gain, health,
                                 cost_a, cost_b, cost_c, cost_d, cost_e))
        elif carried_by == 0:
            my_robot.add_sample(SampleData(sample_id, carried_by, rank, expertise_gain, health,
                                cost_a, cost_b, cost_c, cost_d, cost_e))

    # Write an action using print
    # To debug: print("Debug messages...", file=sys.stderr)

    # COLLECT
    # Is there a better way to do this?
    def collect_constraint(action):
        return action.robot.get_sample_available_storage() > 0
    def collect_priority(action):
        if action.robot.target != SAMPLES:
            return COLLECT_FAR_PRIORITY
        else:
            return COLLECT_PRIORITY
    def collect_execute(action):
        if action.robot.target != SAMPLES:
            return GOTO.format(SAMPLES)
        else:
            # Really simple criterium to choose the rank
            if sum(exp for (mol_type, exp) in my_robot.expertises.items()) > EXPERTISE_TO_SWITCH_TO_3RD_RANK:
                sample_rank = 3 + my_robot.get_rank_correction()
            else:
                sample_rank = 2 + my_robot.get_rank_correction()
            # Rank must be a value in [1,2,3]
            if sample_rank <= 1:
                sample_rank = 1
            elif sample_rank >= 3:
                sample_rank = 3
            return CONNECT.format(sample_rank)
    my_robot.add_action(COLLECT, diagnosis, molecules, collect_constraint, collect_priority, collect_execute)

    # ANALYZE
    def analyze_constraint(action):
        return action.robot.get_undiagnosed_sample() is not None
    def analyze_priority(action):
        if action.robot.target != DIAGNOSIS:
            return ANALYZE_FAR_PRIORITY
        else:
            return ANALYZE_PRIORITY
    def analyze_execute(action):
        if action.robot.target != DIAGNOSIS:
            return GOTO.format(DIAGNOSIS)
        else:
            return CONNECT.format(action.robot.get_undiagnosed_sample().id)
    my_robot.add_action(ANALYZE, diagnosis, molecules, analyze_constraint, analyze_priority, analyze_execute)

    # DOWNLOAD
    def download_constraint(action):
        return action.robot.get_sample_available_storage() > 0 \
               and action.robot.get_best_sample(action.diagnosis_mod.samples, action.molecules_mod.availables) is not None
    def download_priority(action):
        return DOWNLOAD_PRIORITY
    def download_execute(action):
        if action.robot.target != DIAGNOSIS:
            return GOTO.format(DIAGNOSIS)
        else:
            best_sample = action.robot.get_best_sample(action.diagnosis_mod.samples, action.molecules_mod.availables)
            return CONNECT.format(best_sample.id)
    my_robot.add_action(DOWNLOAD, diagnosis, molecules, download_constraint, download_priority, download_execute)

    # UPLOAD
    def upload_constraint(action):
        return any(not s.is_undiagnosed() for s in action.robot.samples) \
               and not any([    collect_constraint(action),
                                analyze_constraint(action),
                                download_constraint(action),
                                gather_constraint(action),
                                produce_constraint(action) ])
    def upload_priority(action):
        return UPLOAD_PRIORITY
    def upload_execute(action):
        if action.robot.target != DIAGNOSIS:
            return GOTO.format(DIAGNOSIS)
        else:
            my_robot.fail()
            first_sample = action.robot.get_first_sample()
            return CONNECT.format(first_sample.id)
    my_robot.add_action(UPLOAD, diagnosis, molecules, upload_constraint, upload_priority, upload_execute)

    # GATHER
    def gather_constraint(action):
        return action.robot.get_molecule_available_storage() > 0 \
               and action.robot.enough_molecules_to_produce_my_sample(action.molecules_mod.availables) is not None \
               and action.robot.get_next_needed_available_molecule(action.molecules_mod.availables) is not None
    def gather_priority(action):
        return GATHER_PRIORITY
    def gather_execute(action):
        needed_available_molecule = action.robot.get_next_needed_available_molecule(action.molecules_mod.availables)
        if action.robot.target != MOLECULES:
            return GOTO.format(MOLECULES)
        else:
            return CONNECT.format(needed_available_molecule)
    my_robot.add_action(GATHER, diagnosis, molecules, gather_constraint, gather_priority, gather_execute)

    # PRODUCE
    def produce_constraint(action):
        return action.robot.get_my_sample_ready_to_produce() is not None
    def produce_priority(action):
        return PRODUCE_PRIORITY
    def produce_execute(action):
        if action.robot.target != LABORATORY:
            return GOTO.format(LABORATORY)
        else:
            my_robot.success()
            ready_sample = action.robot.get_my_sample_ready_to_produce()
            return CONNECT.format(ready_sample.id)
    my_robot.add_action(PRODUCE, diagnosis, molecules, produce_constraint, produce_priority, produce_execute)

    # DO SOMETHING
    print(my_robot.do_something())